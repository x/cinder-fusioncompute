============
Installation
============

At the command line::

    $ pip install cinder-fusioncompute

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv cinder-fusioncompute
    $ pip install cinder-fusioncompute
